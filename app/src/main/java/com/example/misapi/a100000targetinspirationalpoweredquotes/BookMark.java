package com.example.misapi.a100000targetinspirationalpoweredquotes;

/**
 * Created by misapi on 9/21/17.
 */

public class BookMark {

    int id;
    String name;
    int page_number;
    int scroll_nubmer;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPage_number() {
        return page_number;
    }

    public void setPage_number(int page_number) {
        this.page_number = page_number;
    }

    public int getScroll_nubmer() {
        return scroll_nubmer;
    }

    public void setScroll_nubmer(int scroll_nubmer) {
        this.scroll_nubmer = scroll_nubmer;
    }
}
