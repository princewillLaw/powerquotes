package com.example.misapi.a100000targetinspirationalpoweredquotes;

/**
 * Created by Princewill on 11/21/2017.
 */

public class Topics {

    private String name = "";
    private int page_no = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPage_no() {
        return page_no;
    }

    public void setPage_no(int page_no) {
        this.page_no = page_no;
    }
}
