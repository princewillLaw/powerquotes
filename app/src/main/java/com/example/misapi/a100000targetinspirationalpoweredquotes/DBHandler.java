package com.example.misapi.a100000targetinspirationalpoweredquotes;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 3537 on 06-11-2015.
 */
public class DBHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "poweredQuotes";

    // Page table name
    private static final String TABLE_PAGES_DETAIL = "pages";

    // Page Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_PAGE_NO = "page_number";
    private static final String KEY_PAGE_TEXT = "page_text";

    //Settings table name
    private static final String TABLE_SETTINGS_DETAIL = "settings";

    //Settings Table Columns names
    private static final String KEY_FONT_SIZE = "font_size";
    private static final String KEY_FONT_FACE = "font_face";
    private static final String KEY_FONT_BOLD = "font_bold";
    private static final String KEY_FONT_ITALIC = "font_italic";
    private static final String KEY_FONT_UNDERLINED = "font_underlined";

    //BookMarks table name
    private static final String TABLE_BOOKMARKS_DETAIL = "bookmarks";

    //BookMarks Table Columns names
    private  static final String KEY_NAME = "name";
    private  static final String KEY_SCROLL_NO = "scroll_number";

    //Topics table name
    private static final String TABLE_TOPICS_DETAIL = "topics";

    //Topic uses name and page number


    public DBHandler(Context contex) {
        super(contex, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_PAGE_DETAIL_TABLE = "CREATE TABLE " + TABLE_PAGES_DETAIL + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_PAGE_NO + " INTEGER,"
                + KEY_PAGE_TEXT + " TEXT " + ")";

        db.execSQL(CREATE_PAGE_DETAIL_TABLE);

        String CREATE_SETTINGS_DETAIL_TABLE = "CREATE TABLE " + TABLE_SETTINGS_DETAIL + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_FONT_SIZE + " INTEGER,"
                + KEY_FONT_FACE + " TEXT,"
                + KEY_FONT_BOLD + " TEXT,"
                + KEY_FONT_ITALIC + " TEXT,"
                + KEY_FONT_UNDERLINED + " TEXT " + ")";

        db.execSQL(CREATE_SETTINGS_DETAIL_TABLE);

        String CREATE_BOOKMARKS_DETAIL_TABLE = "CREATE TABLE " + TABLE_BOOKMARKS_DETAIL + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAME + " TEXT,"
                + KEY_PAGE_NO + " INTEGER,"
                + KEY_SCROLL_NO + " INTEGER" + ")";

        db.execSQL(CREATE_BOOKMARKS_DETAIL_TABLE);

        String CREATE_TOPICS_DETAIL_TABLE = "CREATE TABLE " + TABLE_TOPICS_DETAIL + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_NAME + " TEXT,"
                + KEY_PAGE_NO + " INTEGER" + ")";

        db.execSQL(CREATE_TOPICS_DETAIL_TABLE);

        System.out.println("db created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAGES_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SETTINGS_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BOOKMARKS_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TOPICS_DETAIL);

        // Create tables again
        onCreate(db);
    }

    void dropDatabase(Context context){
        context.deleteDatabase(DATABASE_NAME);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new Page Information
    void addNewPage(int page_number, String page_text) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_PAGE_NO, page_number);
        values.put(KEY_PAGE_TEXT, page_text);


        // Inserting Row
        db.insert(TABLE_PAGES_DETAIL, null, values);
        db.close(); // Closing database connection

        System.out.println("page "+page_number+" added");
    }

    //add new settings
    void addNewSetting(){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_FONT_SIZE, Settings.getFontsize());
        values.put(KEY_FONT_FACE, Settings.getFontface());
        values.put(KEY_FONT_BOLD, Settings.isBold());
        values.put(KEY_FONT_ITALIC, Settings.isItalic());
        values.put(KEY_FONT_UNDERLINED, Settings.isUnderlined());


        // Inserting Row
        db.insert(TABLE_SETTINGS_DETAIL, null, values);
        db.close(); // Closing database connection

        System.out.println("settings updated");

    }

    //add new bookmark
    void addNewBookmark(String bookmark_name, int page_number, int scroll_number){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_NAME, bookmark_name);
        values.put(KEY_PAGE_NO, page_number);
        values.put(KEY_SCROLL_NO, scroll_number);

        // Inserting Row
        db.insert(TABLE_BOOKMARKS_DETAIL, null, values);
        db.close(); // Closing database connection

        System.out.println("bookmark "+bookmark_name+" added for page "+page_number+" and scroll "+scroll_number);

    }

    // Adding new topic
    void addNewTopic(String  topic_name, int page_no) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_NAME, topic_name);
        values.put(KEY_PAGE_NO, page_no);


        // Inserting Row
        db.insert(TABLE_TOPICS_DETAIL, null, values);
        db.close(); // Closing database connection

        System.out.println("TOPIC: "+topic_name+" added");
    }

    public boolean updatePageInfo(int updId, int pageNo, String pageText) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues args = new ContentValues();

        args.put(KEY_PAGE_NO, pageNo);
        args.put(KEY_PAGE_TEXT, pageText);

        return db.update(TABLE_PAGES_DETAIL, args, KEY_ID + "=" + updId, null) > 0;
    }

    public boolean updateSettings() {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_FONT_SIZE, Settings.getFontsize());
        values.put(KEY_FONT_FACE, Settings.getFontface());
        values.put(KEY_FONT_BOLD, Settings.isBold());
        values.put(KEY_FONT_ITALIC, Settings.isItalic());
        values.put(KEY_FONT_UNDERLINED, Settings.isUnderlined());

        return db.update(TABLE_SETTINGS_DETAIL, values, KEY_ID + "=" + 1, null) > 0;
    }

    public boolean deletePage(int delID){

        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(TABLE_PAGES_DETAIL, KEY_PAGE_NO + "=" + delID, null) > 0;

    }

    public boolean deleteBookmark(int delID){

        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(TABLE_BOOKMARKS_DETAIL, KEY_ID + "=" + delID, null) > 0;

    }

    public String getPage(int page_number){
        String selectQuery = "SELECT * FROM "+ TABLE_PAGES_DETAIL +" WHERE "+ KEY_PAGE_NO +" = "+ page_number;
        String text ="";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                System.out.println("Page info"+cursor.getString(2));
                text = cursor.getString(2);
            }while(cursor.moveToNext());
        }else{
            System.out.println("no page info");
        }

        cursor.close();
        return text;

    }

    public void getSettings(){
        String selectQuery = "SELECT * FROM "+ TABLE_SETTINGS_DETAIL +" WHERE "+ KEY_ID +" = "+ 1;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                Settings.setFontsize(Integer.parseInt(cursor.getString(1)));
                Settings.setFontface(cursor.getString(2));
                Settings.setIsBold(Boolean.parseBoolean(cursor.getString(3)));
                Settings.setIsItalic(Boolean.parseBoolean(cursor.getString(4)));
                Settings.setIsUnderlined(Boolean.parseBoolean(cursor.getString(5)));
                System.out.println("Settings received");
            }while(cursor.moveToNext());
        }else{
            System.out.println("No Settings!!!");
        }
        cursor.close();
    }

    public int getlastPageNo(){
        String selectQuery = "SELECT "+ KEY_PAGE_NO +" FROM "+ TABLE_PAGES_DETAIL;
        int number=0;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                number =  Integer.parseInt(cursor.getString(0));
            }while(cursor.moveToNext());
        }
        System.out.println("last page number: "+number);
        return number;
    }

    // Getting All Topics
    public ArrayList<Topics> getAllTopicsList() {


        ArrayList<Topics> TopicsList = new ArrayList<Topics>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_TOPICS_DETAIL;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Topics topic = new Topics();
                topic.setName(cursor.getString(1));
                topic.setPage_no(cursor.getInt(2));
                // Adding page to list
                TopicsList.add(topic);

            } while (cursor.moveToNext());
        }

        // return contact list
        return TopicsList;
    }

    // Getting All Pages
    public List<String> getAllPagesList() {


        List<String> PagesList = new ArrayList<String>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_PAGES_DETAIL;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                // Adding page to list
                PagesList.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }

        // return contact list
        return PagesList;
    }

    // Getting All BookMark
    public ArrayList<BookMark> getAllBookmarksList() {


        ArrayList<BookMark> BookMarkList = new ArrayList<BookMark>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_BOOKMARKS_DETAIL;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                // Adding page to list
                BookMark bm = new BookMark();
                bm.setId(cursor.getInt(0));
                bm.setName(cursor.getString(1));
                bm.setPage_number(cursor.getInt(2));
                bm.setScroll_nubmer(cursor.getInt(3));

                BookMarkList.add(bm);

            } while (cursor.moveToNext());
        }

        // return contact list
        return BookMarkList;
    }


}