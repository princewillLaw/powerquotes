package com.example.misapi.a100000targetinspirationalpoweredquotes;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

/**
 * Created by misapi on 9/20/17.
 */

public class Settings {

       static int fontsize;
       static String fontface = "";
       static boolean isItalic, isBold, isUnderlined;

       static BookMark bookmark;
       static ArrayList<Integer> pageScrollPosition = new ArrayList<>();

        static int recent_page;
        static int recent_scroll;

        static boolean isDay;

    public static boolean isDay() {
        return isDay;
    }

    public static void setIsDay(boolean isDay) {
        Settings.isDay = isDay;
    }

    public static int getRecent_page() {
        return recent_page;
    }

    public static void setRecent_page(int recent_page) {
        Settings.recent_page = recent_page;
    }

    public static int getRecent_scroll() {
        return recent_scroll;
    }

    public static void setRecent_scroll(int recent_scroll) {
        Settings.recent_scroll = recent_scroll;
    }

    public static int getFontsize() {
        return fontsize;
    }

    public static void setFontsize(int fontsize) {
        Settings.fontsize = fontsize;
    }

    public static String getFontface() {
        return fontface;
    }

    public static void setFontface(String fontface) {
        Settings.fontface = fontface;
    }

    public static boolean isItalic() {
        return isItalic;
    }

    public static void setIsItalic(boolean isTalic) {
        Settings.isItalic = isTalic;
    }

    public static boolean isBold() {
        return isBold;
    }

    public static void setIsBold(boolean isBold) {
        Settings.isBold = isBold;
    }

    public static boolean isUnderlined() {
        return isUnderlined;
    }

    public static void setIsUnderlined(boolean isUnderlined) {
        Settings.isUnderlined = isUnderlined;
    }

    public static BookMark getBookmark() {
        return bookmark;
    }

    public static void setBookmark(BookMark bookmark) {
        Settings.bookmark = bookmark;
    }

    public static int getPageScrollPosition(int pageIndex) {
        int scrollto = 0;
        scrollto = pageScrollPosition.get(pageIndex-1);
        return scrollto;
    }

    public static void setPageScrollPosition(int pageIndex,int scrollPosition) {
        pageScrollPosition.set(pageIndex-1,scrollPosition);

    }

}
