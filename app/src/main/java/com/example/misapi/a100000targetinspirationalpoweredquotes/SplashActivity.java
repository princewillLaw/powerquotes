package com.example.misapi.a100000targetinspirationalpoweredquotes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

public class SplashActivity extends AppCompatActivity {

    TextView title;

    DBHandler db;

    Vector<Sayings> sayings = new Vector<Sayings>();
    Vector<Sayings> fullsayings = new Vector<Sayings>();
    Sayings tempSayings = new Sayings();
    int index=1;
    final int NUMBEROFSAYINGSTODISPLAY = 3;

    int displayedSayings = NUMBEROFSAYINGSTODISPLAY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        title = (TextView)findViewById(R.id.tv_title);

        db = new DBHandler(this);

        //if database does not have pages read them from file
        if(db.getPage(1) == "") {
            //run animation here
            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.blink);
            title.startAnimation(animation);
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        readFile(getAssets().open("new 33333.txt"));
                        savePageTexts();

                        Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        }else{
            Intent intent = new Intent(SplashActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void readFile(InputStream is){
        try {

            if (is != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(is);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";

                while ((receiveString = bufferedReader.readLine()) != null) {
                    processor(receiveString);
                }

                is.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("done with reading file");

    }

    private void processor(String line){

        //remove all white spaces at the start and end of line
        line.trim();

        //check if the line has characters a-z || A-Z || 0-9
        if(line.matches(".*[A-Za-z0-9]+.*") && line.equals(line.toUpperCase())){
            //its a topic
            tempSayings.setTopic(line);
        }else if(line.matches(".*[A-Za-z0-9]+.*") && index == 1){
            //its a quote
            tempSayings.setQuotes(line);
            if(!tempSayings.hasTopic())
                tempSayings.setTopic(sayings.lastElement().getTopic());

            index++;
        }else if(index==2 && line.startsWith("_")){
            //its a author
            tempSayings.setAuthor(line);
            index++;

        }else if(index==3){
            //its a verse
            tempSayings.setVerse(line);
            fullsayings.add(tempSayings);
            sayings.add(tempSayings);
            tempSayings = new Sayings();
            index=1;
        }

    }

    private void savePageTexts(){
        int pageNumber=1;
        int topicIndex=0;
        String currentTopic = "";
        String page = "";
        //for all sayings in sayings vector
        for(Sayings say:sayings){
            //add topic to page
            if(!currentTopic.equals(say.getTopic())){
                if(topicIndex>displayedSayings){
                    db.addNewPage(pageNumber,page);
                    page="";
                    pageNumber++;
                    topicIndex=0;
                }
                db.addNewTopic(say.getTopic(),pageNumber);
                page += "<h2>"+say.getTopic()+"</h2>";
                page += say.getQuotes()+"<br>";
                page += say.getAuthor()+"<br>";
                page += say.getVerse()+"<br><br>";
                currentTopic = say.getTopic();
                topicIndex++;
            }
            else{
                page += say.getQuotes()+"<br>";
                page += say.getAuthor()+"<br>";
                page += ""+say.getVerse()+"<br><br>";
            }
        }
        db.addNewPage(pageNumber,page);
    }

}
