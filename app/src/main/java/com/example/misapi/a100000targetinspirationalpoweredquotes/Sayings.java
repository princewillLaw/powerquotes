package com.example.misapi.a100000targetinspirationalpoweredquotes;

import java.util.Vector;

/**
 * Created by misapi on 9/14/17.
 */

public class Sayings {

    String Topic="",Quotes="",Author="",Verse="";

    public String getTopic() {
        return Topic;
    }

    public void setTopic(String topic) {
        Topic = topic;
    }

    public boolean hasTopic() {
        return !getTopic().isEmpty();
    }

    public String getQuotes() {
        return Quotes;
    }

    public void setQuotes(String quotes) {
        Quotes = quotes;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getVerse() {
        return Verse;
    }

    public void setVerse(String verse) {
        Verse = verse;
    }
}
