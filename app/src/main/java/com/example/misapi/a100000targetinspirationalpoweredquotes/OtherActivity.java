package com.example.misapi.a100000targetinspirationalpoweredquotes;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class OtherActivity extends AppCompatActivity {

    TextView info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other);

        info = (TextView)findViewById(R.id.tv_info);

        setinfoSettings();

    }

    void setinfoSettings(){
        info.setTextSize(Settings.getFontsize());
        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/"+Settings.getFontface());

        info.setTypeface(face,Typeface.NORMAL);

        if(Settings.isBold() && Settings.isItalic())
            info.setTypeface(face,Typeface.BOLD_ITALIC);
        else if(Settings.isBold())
            info.setTypeface(face,Typeface.BOLD);
        else if(Settings.isItalic())
            info.setTypeface(face,Typeface.ITALIC);

        if(Settings.isUnderlined())
            info.setPaintFlags(info.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        else
            info.setPaintFlags(info.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));

        info.setText(info.getText());

    }
}
