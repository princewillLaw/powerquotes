package com.example.misapi.a100000targetinspirationalpoweredquotes;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.ArrayList;

public class BookMarksActivity extends AppCompatActivity {

    ListView bookmarks;
    DBHandler db;

    ArrayList<BookMark> bookmarkList = new ArrayList<BookMark>();
    ArrayList<String> bookmarknames = new ArrayList<>();
    ArrayAdapter adapter;

    public void setListItems(){
        bookmarkList = db.getAllBookmarksList();

        bookmarknames.clear();

        for(BookMark bm:bookmarkList){
            bookmarknames.add(bm.getName());
        }
    }

    public void deleteDialog(final int i){
        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(this);
        deleteDialog.setMessage("Delete this bookmark");
        deleteDialog.setCancelable(true);

        deleteDialog.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        db.deleteBookmark(bookmarkList.get(i).getId());

                        setListItems();
                        adapter.notifyDataSetChanged();
                    }
                });

        deleteDialog.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alertDialog = deleteDialog.create();
        alertDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_marks);

        db = new DBHandler(this);

        setListItems();

        adapter = new ArrayAdapter<String>(this, R.layout.list_view_item_bookmarks, bookmarknames);
        bookmarks = (ListView)findViewById(R.id.lv_bookmarks);
        bookmarks.setAdapter(adapter);

        bookmarks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    BookMark bm = bookmarkList.get(i);
                    Settings.setBookmark(bm);
                    finish();
            }
        });

        bookmarks.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view,int i, long l) {
                deleteDialog(i);
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_book_marks_activity,menu);

        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView)item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //handle presses on the action bar items
        switch (item.getItemId()) {

            case R.id.action_search:

                //run audio code here

                return true;


        }
        return super.onOptionsItemSelected(item);
    }

}
