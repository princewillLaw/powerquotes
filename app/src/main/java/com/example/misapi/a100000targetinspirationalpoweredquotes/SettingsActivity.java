package com.example.misapi.a100000targetinspirationalpoweredquotes;

import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;

public class SettingsActivity extends AppCompatActivity {

    SeekBar textsize;
    RadioGroup fontfamily;
    RadioButton roboto,lobster,rouge,ubuntu,rajdhani,catamaran;
    CheckBox bold, italic,underlined;
    Button foreword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        textsize = (SeekBar)findViewById(R.id.sb_size);

        fontfamily = (RadioGroup)findViewById(R.id.rg_face);

        roboto = (RadioButton)findViewById(R.id.rb_roboto);
        lobster = (RadioButton)findViewById(R.id.rb_lobster);
        rouge = (RadioButton)findViewById(R.id.rb_rougescript);
        ubuntu = (RadioButton)findViewById(R.id.rb_ubuntu);
        rajdhani = (RadioButton)findViewById(R.id.rb_rajdhani);
        catamaran = (RadioButton)findViewById(R.id.rb_catamaran);

        bold = (CheckBox)findViewById(R.id.cb_bold);
        italic = (CheckBox)findViewById(R.id.cb_italic);
        underlined = (CheckBox)findViewById(R.id.cb_underlined);
        foreword = (Button)findViewById(R.id.btn_foreword);

        textsize.setProgress((Settings.getFontsize()-10)*5);
        textsize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Settings.setFontsize(10 +(i/5));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        if(Settings.getFontface().equals("Roboto-Regular.ttf"))
            roboto.setChecked(true);
        if(Settings.getFontface().equals("Lobster-Regular.ttf"))
            lobster.setChecked(true);
        if(Settings.getFontface().equals("RougeScript-Regular.ttf"))
            rouge.setChecked(true);
        if(Settings.getFontface().equals("UbuntuCondensed-Regular.ttf"))
            ubuntu.setChecked(true);
        if(Settings.getFontface().equals("Rajdhani-Regular.ttf"))
            rajdhani.setChecked(true);
        if(Settings.getFontface().equals("Catamaran-Regular.ttf"))
            catamaran.setChecked(true);

        fontfamily.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                switch (fontfamily.getCheckedRadioButtonId()){
                    case R.id.rb_roboto:
                        Settings.setFontface("Roboto-Regular.ttf");
                        break;
                    case R.id.rb_lobster:
                        Settings.setFontface("Lobster-Regular.ttf");
                        break;
                    case R.id.rb_rougescript:
                        Settings.setFontface("RougeScript-Regular.ttf");
                        break;
                    case R.id.rb_catamaran:
                        Settings.setFontface("Catamaran-Regular.ttf");
                        break;
                    case R.id.rb_ubuntu:
                        Settings.setFontface("UbuntuCondensed-Regular.ttf");
                        break;
                    case R.id.rb_rajdhani:
                        Settings.setFontface("Rajdhani-Regular.ttf");
                        break;
                }
            }
        });
        if(Settings.isBold())
            bold.setChecked(true);

        bold.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    Settings.setIsBold(true);
                else
                    Settings.setIsBold(false);
            }
        });

        if(Settings.isItalic())
            italic.setChecked(true);
        italic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    Settings.setIsItalic(true);
                else
                    Settings.setIsItalic(false);
            }
        });

        if(Settings.isUnderlined())
            underlined.setChecked(true);
        underlined.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                    Settings.setIsUnderlined(true);
                else
                    Settings.setIsUnderlined(false);
            }
        });


        foreword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingsActivity.this,OtherActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings_activity,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //handle presses on the action bar items
        switch (item.getItemId()) {

            case R.id.action_done:
                DBHandler db = new DBHandler(this);
                db.updateSettings();
                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
