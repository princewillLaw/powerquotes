package com.example.misapi.a100000targetinspirationalpoweredquotes;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

public class MainActivity extends AppCompatActivity {

    ListView page_list;
    String[] page_number_list;
    ArrayAdapter adapter;

    TextView book_view,page_number;
    FloatingActionButton page_up,page_down;
    ImageView share,bookmark,daynight,settings;
    ScrollView scroller;

    int pageIndex = 1;
    int pageMaximum = 0;

    DBHandler db;

    //Search variables
    ArrayAdapter<String> Searchadapter;
    List<String> filtered_topics_names;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        page_list = (ListView) findViewById(R.id.lv_page_numbers);
        book_view = (TextView) findViewById(R.id.tv_book_holder);
        page_number = (TextView) findViewById(R.id.tv_page_number);
        page_up = (FloatingActionButton) findViewById(R.id.fab_latter_page);
        page_down = (FloatingActionButton) findViewById(R.id.fab_former_page);
        share = (ImageView)findViewById(R.id.ib_share);
        bookmark = (ImageView)findViewById(R.id.ib_bookmark);
        daynight = (ImageView)findViewById(R.id.ib_day_night);
        settings = (ImageView)findViewById(R.id.ib_settings);
        scroller = (ScrollView)findViewById(R.id.sv_scroller);

        db = new DBHandler(this);

        //get last page number
        pageMaximum = db.getlastPageNo();
        //set all page scroll to default zero
        for(int i=0;i<pageMaximum;i++)
            Settings.pageScrollPosition.add(1);

        //gets settings from database or set default
        databaseSettingsSetup();
        //use settings to display textview
        setBookViewSettings();

        //setpagefromDB();
        //setting the page list display
        page_number_list = new String[pageMaximum];
        for(int i=0;i<pageMaximum;i++){
            page_number_list[i] = (i+1)+"";
        }

        adapter = new ArrayAdapter<String>(this,R.layout.list_view_item_pages,page_number_list);
        page_list.setAdapter(adapter);
        page_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Settings.setPageScrollPosition(pageIndex,scroller.getScrollY());
                pageIndex=i+1;
                setpagefromDB();
                page_list.setVisibility(View.GONE);
            }
        });

        page_list.setVisibility(View.GONE);

        book_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                page_list.setVisibility(View.GONE);
            }
        });

        //On button Clicks
        page_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            onButtonClick(page_up.getId());
            }
        });
        page_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            onButtonClick(page_down.getId());
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {onButtonClick(share.getId());
            }
        });
        daynight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {onButtonClick(daynight.getId()); }
        });
        bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {onButtonClick(bookmark.getId()); }
        });
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {onButtonClick(settings.getId()); }
        });
        page_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {onButtonClick(page_number.getId()); }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //handle presses on the action bar items
        switch (item.getItemId()) {

            case R.id.action_search:

                //open search dialog
                ViewDialog viewDialog = new ViewDialog();
                final Dialog dialog = viewDialog.setDialog(MainActivity.this,R.layout.dialog_search_topics);

                // set the custom dialog components - text, image and button
                EditText search_filter = (EditText) dialog.findViewById(R.id.et_search);
                ListView topics = (ListView) dialog.findViewById(R.id.lv_topics);

                final ArrayList<Topics> topic_list = db.getAllTopicsList();
                final String[] full_topics_names = new String[((topic_list.size()/4)+1)];

                for(int s=0;s<full_topics_names.length;s++)full_topics_names[s] = "";

                int x = 0;
                int count = 1;

                for(Topics i:topic_list){
                    full_topics_names[x] += i.getName()+"\n\n";
                    count++;
                    if(count==5){
                        count = 1;
                        x++;
                    }
                }

                //creating and filling the filtered list of topics
                filtered_topics_names = new ArrayList<String>();
                for(int i=0;i<full_topics_names.length;i++){
                    filtered_topics_names.add(full_topics_names[i]);
                }

                Searchadapter = new ArrayAdapter<String>(this,R.layout.list_view_item_topics,filtered_topics_names);
                topics.setAdapter(Searchadapter);
                topics.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Settings.setPageScrollPosition(pageIndex,scroller.getScrollY());

                        for(Topics topic:topic_list){
                            if(Searchadapter.getItem(i).contains(topic.getName()))
                                pageIndex = topic.getPage_no();
                        }
                        setpagefromDB();
                        dialog.dismiss();
                    }
                });
                search_filter.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        //empty array to store new values
                       filtered_topics_names.clear();
                        //loop through topics and filter out
                        for(String t:full_topics_names){
                            if(t.contains(charSequence.toString().toUpperCase())){
                                filtered_topics_names.add(t);
                            }
                        }

                        Searchadapter.notifyDataSetChanged();
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                dialog.show();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        saveState();
        super.onPause();
    }

    @Override
    protected void onPostResume() {
        setBookViewSettings();
        if(Settings.getBookmark() != null){
            pageIndex = Settings.getBookmark().getPage_number();
            System.out.println("its from bookmark");
            setpagefromDB();
        }
        db.updateSettings();
        super.onPostResume();
    }

    public void onButtonClick(int button_id){
        switch(button_id){

            case R.id.fab_former_page:
                    if ((pageIndex) > 1) {
                        //Save the scrolled position in the settings in percent
                        Settings.setPageScrollPosition(pageIndex,scroller.getScrollY());
                        pageIndex--;
                        setpagefromDB();
                    }
                break;

            case R.id.fab_latter_page:
                    if(pageMaximum>(pageIndex)){

                        //Save the scrolled position in the settings in percent
                        Settings.setPageScrollPosition(pageIndex,scroller.getScrollY());
                        pageIndex++;
                        setpagefromDB();
                    }
                break;

            case R.id.ib_share:

                break;

            case R.id.ib_day_night:

                //run day/night switch code here
                Settings.setIsDay(!Settings.isDay());
                BookViewDayNight();

                break;

            case R.id.ib_bookmark:

                final int bookmarkScroll = scroller.getScrollY();

                AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                builder1.setMessage("Book Marks");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "New",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                ViewDialog alert = new ViewDialog();
                                final Dialog newBookmarkDialog = alert.setDialog(MainActivity.this,R.layout.dialog_add_bookmark);

                                final EditText name = (EditText) newBookmarkDialog.findViewById(R.id.et_bookmark_name);
                                final TextView page = (TextView) newBookmarkDialog.findViewById(R.id.tv_bookmark_page);

                                page.setText(page.getText()+" "+pageIndex);

                                Button save = (Button) newBookmarkDialog.findViewById(R.id.btn_save);
                                save.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if(!name.getText().toString().equals("")){
                                            db.addNewBookmark(name.getText().toString(),pageIndex,bookmarkScroll);
                                        }else{
                                            db.addNewBookmark("untitled "+db.getAllBookmarksList().size(),pageIndex,bookmarkScroll);
                                        }
                                        scroller.scrollTo(0,bookmarkScroll);
                                        newBookmarkDialog.dismiss();
                                    }
                                });
                                newBookmarkDialog.show();

                                dialog.cancel();
                            }
                        });

                builder1.setNegativeButton(
                        "Open",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent bookmark_intent = new Intent(MainActivity.this, BookMarksActivity.class);
                                startActivity(bookmark_intent);
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

                break;

            case R.id.ib_settings:
                Intent settings_intent = new Intent(MainActivity.this,SettingsActivity.class);
                startActivity(settings_intent);
                break;

            case R.id.tv_page_number:
                if(page_list.getVisibility()== View.VISIBLE){
                    page_list.setVisibility(View.GONE);
                }else{
                    page_list.setVisibility(View.VISIBLE);
                }

                break;

        }
    }

    private void BookViewDayNight(){
        if(!Settings.isDay()){
            book_view.setBackgroundColor(Color.BLACK);
            scroller.setBackgroundColor(Color.BLACK);
            book_view.setTextColor(Color.WHITE);
        }else{
            book_view.setBackgroundColor(Color.TRANSPARENT);
            scroller.setBackgroundColor(Color.TRANSPARENT);
            book_view.setTextColor(Color.BLACK);
        }
    }

    private void setpagefromDB(){
        book_view.setVisibility(View.INVISIBLE);
        page_number.setText(pageIndex+"/"+pageMaximum);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            book_view.setText(Html.fromHtml(db.getPage(pageIndex),Html.FROM_HTML_MODE_LEGACY));
        } else {
            book_view.setText(Html.fromHtml(db.getPage(pageIndex)));
        }

        scroller.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(Settings.getBookmark() != null){
                    scroller.scrollTo(0,Settings.getBookmark().getScroll_nubmer());
                    System.out.println("we get the bookmark page "+pageIndex+" to scroll to "+Settings.getBookmark().getScroll_nubmer());
                    Settings.setBookmark(null);
                }else{
                    scroller.scrollTo(0,Settings.getPageScrollPosition(pageIndex));
                }
                book_view.setVisibility(View.VISIBLE);
            }
        },200);

    }

    private void databaseSettingsSetup(){
        db.getSettings();
        if(Settings.getFontface().equals("")){
            Settings.setFontsize(20);
            Settings.setFontface("Roboto-Regular.ttf");
            Settings.setIsBold(false);
            Settings.setIsItalic(false);
            Settings.setIsUnderlined(false);
        }
        db.addNewSetting();
    }

    private void setBookViewSettings(){
        readState();
        book_view.setVisibility(View.INVISIBLE);
        book_view.setTextSize(Settings.getFontsize());
        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/"+Settings.getFontface());

            book_view.setTypeface(face,Typeface.NORMAL);

        if(Settings.isBold() && Settings.isItalic())
            book_view.setTypeface(face,Typeface.BOLD_ITALIC);
        else if(Settings.isBold())
            book_view.setTypeface(face,Typeface.BOLD);
        else if(Settings.isItalic())
            book_view.setTypeface(face,Typeface.ITALIC);

        if(Settings.isUnderlined())
            book_view.setPaintFlags(book_view.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        else
            book_view.setPaintFlags(book_view.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));

        book_view.setText(book_view.getText());

        scroller.postDelayed(new Runnable() {
            @Override
            public void run() {
                scroller.scrollTo(0,Settings.getRecent_scroll());
                book_view.setVisibility(View.VISIBLE);
            }
        },200);

    }

    private void saveState(){
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("recent_page", pageIndex);
        editor.putInt("recent_scroll", scroller.getScrollY());
        editor.putBoolean("isDay",Settings.isDay());
        editor.commit();
    }

    private void readState(){
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        Settings.setIsDay(sharedPref.getBoolean("isDay",true));
        BookViewDayNight();
        Settings.setRecent_page(sharedPref.getInt("recent_page", pageIndex));

        pageIndex = Settings.getRecent_page();
        setpagefromDB();

        Settings.setRecent_scroll(sharedPref.getInt("recent_scroll", scroller.getScrollY()));
    }

    public class ViewDialog {

        public Dialog setDialog(Activity activity, int layout){
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(layout);

            return dialog;

        }
    }

}
